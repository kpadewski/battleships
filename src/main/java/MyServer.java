import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class MyServer {

    int port;
    String file;
    Scanner standardInput;
    String player;
    String mode;

    String host;

    MyServer(String mode, String port, String file, String player) throws IOException {
        this.port = Integer.parseInt(port);
        this.file = file;
        this.player = player;
        this.mode = mode;
        standardInput = new Scanner(System.in);
    }

    MyServer(String mode, String port, String file, String player, String host) throws IOException {
        this(mode, port, file, player);
        this.host = host;
    }

    public void playBattleships() throws IOException {

        //POWTARZA SIE
        MyShipsMap myMap = new MyShipsMap(file);
        OpponentShipsMap oppMap = new OpponentShipsMap();
        int rowIaim = 0;
        int colIaim = 0;

        try {
            Socket opponent;
            if (mode.equals("server")) {
                ServerSocket s = new ServerSocket(port);
                opponent = s.accept();
            }
            else  opponent = new Socket(host, port);


            //POWT
            InputStream inStream = opponent.getInputStream();
            OutputStream outStream = opponent.getOutputStream();
            try (var in = new Scanner(inStream, StandardCharsets.UTF_8)) {
                var out = new PrintWriter( new OutputStreamWriter(outStream, StandardCharsets.UTF_8),true);
                var done = false;

                boolean firstMove;
                if (mode.equals("server")) firstMove = false;
                else firstMove = true;

                //POWT
                while (!done) {
                    String messageFromOpponent; String[] messageSplit; String messageToOpponent;

                    if (!firstMove) {

                        //POWT
                        messageFromOpponent = in.nextLine();
                        System.out.println("\nWiadomosc od przeciwnika: " + messageFromOpponent);
                        if (messageFromOpponent.trim().equals("ostatni zatopiony")) {
                            System.out.println("Wygrana");
                            done = true;
                            oppMap.reaction(rowIaim, colIaim, messageFromOpponent);
                            lastPrintMapsWon(myMap, oppMap);
                            continue;
                        }
                        messageSplit = messageFromOpponent.split(";");
                        int col = messageSplit[1].charAt(0) - 64;
                        int row = Integer.parseInt(messageSplit[1].substring(1));
                        oppMap.reaction(rowIaim, colIaim, messageSplit[0]);
                        messageToOpponent = myMap.reaction(row, col);
                        if (messageToOpponent.equals("ostatni zatopiony")){
                            done = true;
                            System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent);
                            System.out.println("Przegrana");
                            out.println(messageToOpponent);
                            lastPrintMaps(myMap, oppMap);
                            continue;
                        }
                    }
                    else messageToOpponent = "start;";
                    firstMove = false;

                    //POWT
                    printMaps(myMap, oppMap);
                    System.out.println();
                    System.out.println("Twój ruch: (np. \"A1\")");
                    String whereIAim;
                    if (player.equals("person")) whereIAim = standardInput.nextLine();
                    else whereIAim = oppMap.nextMove();
                    char colChar2 = whereIAim.charAt(0);
                    colIaim = colChar2 - 64;
                    rowIaim = Integer.parseInt(whereIAim.substring(1));
                    messageToOpponent += whereIAim;
                    out.println(messageToOpponent);
                    System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent);
                    System.out.println("Czekaj");
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }














/*        try (var s = new ServerSocket(port)) {
            try (Socket opponent = s.accept()) {

                //POWT
                InputStream inStream = opponent.getInputStream();
                OutputStream outStream = opponent.getOutputStream();

                //POWT
                try (var in = new Scanner(inStream, StandardCharsets.UTF_8)) {
                    var out = new PrintWriter( new OutputStreamWriter(outStream, StandardCharsets.UTF_8),true);
                    var done = false;


                    //POWT
                    while (!done) {
                        String messageFromOpponent; String[] messageSplit; String messageToOpponent;


                        //POWT
                        messageFromOpponent = in.nextLine();
                        System.out.println("\nWiadomosc od przeciwnika: " + messageFromOpponent);
                        if (messageFromOpponent.trim().equals("ostatni zatopiony")) {
                            System.out.println("Wygrana");
                            done = true;
                            oppMap.reaction(rowIaim, colIaim, messageFromOpponent);
                            lastPrintMapsWon(myMap, oppMap);
                            continue;
                        }
                        messageSplit = messageFromOpponent.split(";");
                        int col = messageSplit[1].charAt(0) - 64;
                        int row = Integer.parseInt(messageSplit[1].substring(1));
                        oppMap.reaction(rowIaim, colIaim, messageSplit[0]);
                        messageToOpponent = myMap.reaction(row, col);
                        if (messageToOpponent.equals("ostatni zatopiony")){
                            done = true;
                            System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent);
                            System.out.println("Przegrana");
                            out.println(messageToOpponent);
                            lastPrintMaps(myMap, oppMap);
                            continue;
                        }




                        //POWT
                        printMaps(myMap, oppMap);
                        System.out.println();
                        System.out.println("Twój ruch: (np. \"A1\")");
                        String whereIAim;
                        if (player.equals("person")) whereIAim = standardInput.nextLine();
                        else whereIAim = oppMap.nextMove();
                        char colChar2 = whereIAim.charAt(0);
                        colIaim = colChar2 - 64;
                        rowIaim = Integer.parseInt(whereIAim.substring(1));
                        messageToOpponent += whereIAim;
                        out.println(messageToOpponent);
                        System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent);
                        System.out.println("Czekaj");
                    }
                }
            }
        }*/
    }


    //POWT
    void printMaps(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.print();
        System.out.println();
        System.out.print("Moja mapa:");
        myMap.print();
    }
    void lastPrintMaps(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.lastPrint();
        System.out.println();
        System.out.print("Moja mapa:");
        myMap.lastPrint();
    }
    void lastPrintMapsWon(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.lastPrintWon();
        System.out.println();
        System.out.print("Moja mapa:");
        myMap.lastPrint();
    }
}
