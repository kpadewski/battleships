import java.io.IOException;

public class MyShipsMap {
    char[][] charArray;
    int[][] intArray;
    boolean[] shipsOnWater;
    boolean opponentMissed[][] = new boolean[10][10];

    MyShipsMap(String file) throws IOException {
        charArray = MyMapMethods.getMap(file);
        intArray = MyMapMethods.shipsArray(file);
        shipsOnWater = new boolean[10];
        for (int i = 0; i < 10; i++) shipsOnWater[i] = true;
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) opponentMissed[i][j] = false;
        }
    }

    public String reaction(int inputRow, int inputCol){
        int row = inputRow - 1;
        int col = inputCol - 1;
        if (charArray[row][col] == '.') {
            opponentMissed[row][col] = true;
            return "pudło;";
        }
        else if (charArray[row][col] == '!') {
            int shipNumber = intArray[row][col];
            if (shipsOnWater[shipNumber - 1]) return "trafiony;";
            else return "trafiony zatopiony;";
        }
        else {
            charArray[row][col] = '!';
            int shipNumber = intArray[row][col];
            boolean isSunk = MyMapMethods.checkIfSunk(charArray, intArray, shipNumber);
            if (isSunk){
                shipsOnWater[shipNumber - 1] = false;
                if (allShipsSunk()) return "ostatni zatopiony";
                else return "trafiony zatopiony;";
            }
            else{
                return "trafiony;";
            }
        }
    }

    boolean allShipsSunk (){
        for (int i = 0; i < 10; i++){
            if (shipsOnWater[i]) return false;
        }
        return true;
    }

    public void print(){
        System.out.println();
        for(int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) System.out.print(charArray[i][j]);
            System.out.println();
        }
    }
    public void lastPrint(){
        System.out.println();
        for(int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) {
                if (charArray[i][j] == '.' && opponentMissed[i][j]) System.out.print("~");
                else if (charArray[i][j] == '.' && !opponentMissed[i][j]) System.out.print(".");
                else if (charArray[i][j] == '!') System.out.print("@");
                else System.out.print("#");
            }
            System.out.println();
        }
    }
}
