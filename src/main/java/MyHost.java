import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class MyHost{
    int port;
    String file;
    Scanner standardInput;
    String player;
    String mode;
    String host;

    MyHost(String mode, String port, String file, String player) throws IOException {
        this.port = Integer.parseInt(port);
        this.file = file;
        this.player = player;
        this.mode = mode;
        standardInput = new Scanner(System.in);
    }

    MyHost(String mode, String port, String file, String player, String host) throws IOException {
        this(mode, port, file, player);
        this.host = host;
    }

    public void playBattleships() throws IOException {

        MyShipsMap myMap = new MyShipsMap(file);
        OpponentShipsMap oppMap = new OpponentShipsMap();

        try {
            Socket opponent;
            if (mode.equals("server")) {
                ServerSocket s = new ServerSocket(port);
                opponent = s.accept();
            }
            else opponent = new Socket(host, port);

            InputStream inStream = opponent.getInputStream();
            OutputStream outStream = opponent.getOutputStream();

            try (var in = new Scanner(inStream, StandardCharsets.UTF_8)) {
                var out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_8),true);

                var done = false;
                boolean firstMove;
                if (mode.equals("server")) firstMove = false;
                else firstMove = true;

                int rowIaim = 0; int colIaim = 0;

                while (!done) {
                    int[] retArray = oneRound(firstMove, in, out, rowIaim, colIaim, myMap, oppMap);
                    rowIaim = retArray[0];
                    colIaim = retArray[1];
                    done = retArray[2] > 0;
                    firstMove = false;
                }
            }
        } catch (IOException e) { e.printStackTrace(); }
    }

    int[] oneRound(boolean firstMove, Scanner in, PrintWriter out, int rowIaim, int colIaim, MyShipsMap myMap, OpponentShipsMap oppMap){
        String messageFromOpponent;
        String messageToOpponent;

        if (!firstMove) {
            messageFromOpponent = in.nextLine();
            System.out.println("\nWiadomosc od przeciwnika: " + messageFromOpponent);

            if (messageFromOpponent.trim().equals("ostatni zatopiony"))
                return ostatniZatopionyOpponent(rowIaim, colIaim, myMap, oppMap, messageFromOpponent);

            messageToOpponent = reactionToOpponentMove(rowIaim, colIaim, myMap, oppMap, messageFromOpponent);

            if (messageToOpponent.equals("ostatni zatopiony"))
                return ostatniZatopionyMyHost(rowIaim, colIaim, myMap, oppMap, messageToOpponent, out);
        }
        else messageToOpponent = "start;";

        return myMove(myMap, oppMap, messageToOpponent, out);
    }

    int[] ostatniZatopionyOpponent(int rowIaim, int colIaim, MyShipsMap myMap, OpponentShipsMap oppMap, String messageFromOpponent) {
        System.out.println("Wygrana");
        oppMap.reaction(rowIaim, colIaim, messageFromOpponent);
        lastPrintMapsWon(myMap, oppMap);
        return new int[]{rowIaim, colIaim, 1};
    }

    int[] ostatniZatopionyMyHost(int rowIaim, int colIaim, MyShipsMap myMap, OpponentShipsMap oppMap, String messageToOpponent, PrintWriter out) {
        System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent);
        System.out.println("Przegrana");
        out.println(messageToOpponent);
        lastPrintMaps(myMap, oppMap);
        return new int[]{rowIaim,colIaim,1};
    }

    int[] myMove(MyShipsMap myMap, OpponentShipsMap oppMap, String messageToOpponent, PrintWriter out) {
        printMaps(myMap, oppMap);
        System.out.println("\nTwój ruch: (np. \"A1\")");
        String whereIAim;
        if (player.equals("person")) whereIAim = standardInput.nextLine();
        else whereIAim = oppMap.nextMove();
        char colChar = whereIAim.charAt(0);
        int col = colChar - 64;
        int row = Integer.parseInt(whereIAim.substring(1));
        messageToOpponent += whereIAim;
        out.println(messageToOpponent);
        System.out.println("Wiadomość wyslana do przeciwnika: " + messageToOpponent + "\nCzekaj");
        return new int[]{row, col,0};
    }

    String reactionToOpponentMove(int rowIaim, int colIaim, MyShipsMap myMap, OpponentShipsMap oppMap, String messageFromOpponent){
        String[] messageSplit = messageFromOpponent.split(";");
        int col = messageSplit[1].charAt(0) - 64;
        int row = Integer.parseInt(messageSplit[1].substring(1));
        oppMap.reaction(rowIaim, colIaim, messageSplit[0]);
        return myMap.reaction(row, col);
    }

    void printMaps(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.print();
        System.out.print("\nMoja mapa:");
        myMap.print();
    }
    void lastPrintMaps(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.lastPrint();
        System.out.print("\nMoja mapa:");
        myMap.lastPrint();
    }
    void lastPrintMapsWon(MyShipsMap myMap, OpponentShipsMap oppMap){
        System.out.print("Mapa przeciwnika:");
        oppMap.lastPrintWon();
        System.out.print("\nMoja mapa:");
        myMap.lastPrint();
    }
}
