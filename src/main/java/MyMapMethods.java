import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyMapMethods {

    public static char[][] getMap(String file) throws IOException {
        char[][] map = new char[10][10];
        File inputFile = new File(file);
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));

        String currentLine;
        for (int row = 0; row < 10; row++){
            currentLine = reader.readLine().trim();
            for (int col = 0; col < 10; col++) map[row][col] = currentLine.charAt(col);
        }
        reader.close();
        return map;
    }

    public static int[][] shipsArray(String file) throws IOException {
        char[][] inputMap = getMap(file);
        int[][] outputMap = new int[10][10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                outputMap[i][j] = 0;
            }
        }
        int newNumber = 1;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (inputMap[i][j] == '#'){
                    int number = numberedNeighbour(outputMap, i, j);
                    if (number != 0) outputMap[i][j] = number;
                    else{
                        outputMap[i][j] = newNumber;
                        newNumber++;
                    }
                }
            }
        }
        return outputMap;
    }

    static int numberedNeighbour(int[][] numberedArray, int row, int col){
        for (int i = row - 1; i <= row + 1; i++){
            for (int j = col - 1; j <= col + 1; j++){
                if (!(i == row && j == col) && i >= 0 && i < 10 && j >= 0 && j < 10){
                    if (numberedArray[i][j] != 0) return numberedArray[i][j];
                }
            }
        }
        return 0;
    }

    public static boolean checkIfSunk (char[][] fileArray, int[][] numberArray, int shipNumber){
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (fileArray[i][j] == '#' && numberArray[i][j] == shipNumber) return false;
            }
        }
        return true;
    }
}
