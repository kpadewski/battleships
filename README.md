## Battleships

The game is played between two hosts - a server and a client. The application communicates with another application using TCP protocol and UTF-8 encoding. It can be played by two people, by a person and a computer or between two computers.
To run the application build the gradle project, enter build/libs folder and run java -jar Battleships-1.0-SNAPSHOT.jar and specify parameters.
The application accepts the following parameters:
* -mode [server|client]
* -port Number - port on which the aplication communicates
* -map filename - path to a file containing a map of ships
* -host hostAdress - (nessary to specify if your host is playing in client mode) adress of the server to which you want to connect your client
* -player [computer|person]

### Map
The map saved in a text file should consist of 10 lines of 10 characters each where '.' 
represents a free square and '#' means there's a ship. Board's dimensions are 10x10. 
During the game rows are numbered from 1 to 10 and columns are represented with letters from A to J. 
The board should contain 10 ships:
* 4 ships of size 1,
* 3 ships of size 2,
* 2 ships of size 3,
* 1 ship of size 4.

They can be broken in different ways, but the squares of one ship 
should have at least one common side with the rest of the ship (common corner is not enough).

Sample map:
```
..........
#.........
#..#.....#
..##..#...
#.....#...
..........
...##..#.#
.......#..
.##....#.#
..##......
```

### Moves
In each round you're presented with your opponent's map and your map. As you gather information on the location of your opponent's ships dots on the first map are replaced with letters. Letter 'n' means there is no ship, letter 't' means a ship was hit, and letter T means that a whole ship has sunk. When one of your ships gets hit, a square changes from '#' to '!'.
At the end of the game two maps are printed:
On the first (opponent's map): '.' is water, '#' - a ship, ? - unknown.
On the second one (your map): '~' is water, '@' - a ship, . - not hit.
