public class OpponentShipsMap {

    char[][] array = new char[10][10];
    char[][] whereToAimArray = new char[10][10];

    OpponentShipsMap(){
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++) array[i][j] = '.';
        }
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++) whereToAimArray[i][j] = '.';
        }
    }

    public void reaction(int row, int col, String message){
        if (message.equals("trafiony")) array[row - 1][col - 1] = 't';
        else if (message.equals("pudło")) array[row - 1][col - 1] = 'n';
        else if (message.equals("trafiony zatopiony") || message.equals("ostatni zatopiony")){
            array[row - 1][col - 1] = 'T';
            wholeShipSunk(row - 1, col - 1);
        }
    }

    public void print(){
        System.out.println();
        for(int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) System.out.print(array[i][j]);
            System.out.println();
        }
    }

    public void lastPrint(){
        System.out.println();
        for(int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) {
                if (array[i][j] == '.' && whereToAimArray[i][j] != 'b') System.out.print("?");
                else if (array[i][j] == '.') System.out.print(".");
                else if (array[i][j] == 'n') System.out.print(".");
                else System.out.print("#");
            }
            System.out.println();
        }
    }

    public void lastPrintWon(){
        System.out.println();
        for(int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++) {
                if (array[i][j] == '.') System.out.print(".");
                else if (array[i][j] == 'n') System.out.print(".");
                else System.out.print("#");
            }
            System.out.println();
        }
    }

    void wholeShipSunk(int row, int col){
        int x = -1;
        int y = -1;
        for (int i = row - 1; i <= row + 1; i++){
            for(int j = col - 1; j <= col + 1; j++){
                if(i >= 0 && j >=0 && i < 10 && j < 10){
                    if (array[i][j] == 't') {
                        array[i][j] = 'T';
                        x = i; y = j;
                    }
                }
            }
        }
        if (x != -1) wholeShipSunk(x, y);
    }

    public String nextMove (){
        int returnInt;
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (array[i][j] == 't') {
                    for (int row = i-1; row <= i+1; row++){
                        for (int col = j-1; col <= j+1; col++) {
                            if(row >= 0 && row < 10 && col >=0 && col < 10 && !(row == i && col == j)) whereToAimArray[row][col] = 'g';
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (array[i][j] == 'T') {
                    for (int row = i-1; row <= i+1; row++){
                        for (int col = j-1; col <= j+1; col++) {
                            if(row >= 0 && row < 10 && col >=0 && col < 10 && !(row == i && col == j)) whereToAimArray[row][col] = 'b';
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (array[i][j] != '.') {
                    whereToAimArray[i][j] = 'b';
                }
            }
        }

        int lookingForG = -1;

        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (whereToAimArray[i][j] == 'g') lookingForG = 10*i + j;
            }
        }

        if (lookingForG >= 0) returnInt = lookingForG;
        else{
            returnInt = 0;
            while(true){
                int myMove = (int) ((Math.random() * 100) % 100);
                int row = myMove / 10;
                int col = myMove % 10;
                if (whereToAimArray[row][col] != 'b') {
                    returnInt = myMove;
                    break;
                }
            }
        }
        int retColumn = returnInt % 10;
        int retRow = (returnInt / 10) + 1;
        char charColumn = (char) (retColumn + 65);
        return Character.toString(charColumn) + retRow;
    }
}
