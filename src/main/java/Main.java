import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String mode = "";
        String port = "";
        String file = "";
        String host = "localhost";
        String player = "computer";

        for (int i = 0; i < args.length; i = i+2) {
            if (args[i].equals("-mode")) mode = args[i + 1];
            else if (args[i].equals("-port")) port = args[i + 1];
            else if (args[i].equals("-map")) file = args[i + 1];
            else if (args[i].equals("-host")) host = args[i + 1];
            else if (args[i].equals("-player")) player = args[i + 1];
        }

        MyHost myHost;
        if (mode.equals("server")) myHost = new MyHost(mode, port, file, player);
        else myHost = new MyHost(mode, port, file, player, host);
        myHost.playBattleships();
    }
}
